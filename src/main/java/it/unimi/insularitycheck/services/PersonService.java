package it.unimi.insularitycheck.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import it.unimi.insularitycheck.dal.PeopleDao;
import it.unimi.insularitycheck.dal.TwitterDao;
import it.unimi.insularitycheck.dal.TwitterDriver;
import it.unimi.insularitycheck.model.Person;
import it.unimi.insularitycheck.model.TwitteFollowersIdsResults;
import twitter4j.TwitterException;

public class PersonService {

	final Logger log = LogManager.getLogger();

	@Autowired
	private PeopleDao peopleDao;
	
	@Autowired
	private TwitterDao twitterDao;

	public void importFollowers(int level, int followersCountThreashold) {
    	List<Person> people = peopleDao.getPeopleWithSomeFollowers(level, followersCountThreashold);
    	
    	for (Person p : people) {
    		try {
    			getAndSaveFollowers(p, level+1);
			} catch (TwitterException e) {
				log.error(e);
			}
    	}
	}
	
	public void getAndSaveFollowers(Person p, int level) throws TwitterException {

		long nextCursor = -1;

		do {

			log.info("Get page of followers of {}. Page number: {}", p.getTwitterId(), p.getPageNum());
			int retries = 0;
			TwitteFollowersIdsResults result = null;
			try {

				result = twitterDao.getNextFollowersIdsPageOf(p.getTwitterId(), p.getNextCursor(), p.getPageNum());

			} catch (TwitterException e) {
				log.warn(e);
				if (retries < 3) {
					log.warn("Retry #{}", retries);
					result = twitterDao.getNextFollowersIdsPageOf(p.getTwitterId(), p.getNextCursor(), p.getPageNum());
					retries++;
				} else {
					throw e;
				}
			}

			// salvataggio followers utente

			final TwitteFollowersIdsResults resultFinal = result;

			// salvataggio followers su DB
			saveFollowers(resultFinal.getIds(), p);

			nextCursor = result.getNextCursor();

			p.setPageNum(p.getPageNum() + 1);
			p.setNextCursor(nextCursor);

			peopleDao.savePerson(p);

		} while (nextCursor > 0);

	}

	   
    public void lookUpPeople(int limit) throws TwitterException {
    	
    	log.info("Reading people from neo4j");


    	do {
        	List<Person> people = peopleDao.getPeopleNotLookedUpYet(limit);
        	
    		log.info("People read from neo4j. People count: {}", people.size());
        	
    		if (people == null || people.size() == 0)
    			return;
    		
    		getInfoAndSaveUsers(people);
        	
    	} while (true);
    	
    }

	public void saveFollowers(long[] ids, Person followed) {

		log.info("Start saving followers of: {}. Page number:{}. Number of ids: {}", followed, followed.getPageNum(),
				ids.length);

		log.info("Start opening neo4j session. Leader: {}", followed);

		try {

			List<Map<String, Object>> unwindList = new ArrayList<Map<String, Object>>();

			int count = 0;

			for (int i = 0; i < ids.length; i++) {
				count++;
				long followerId = ids[i];
				Map<String, Object> unwindData = new HashMap<String, Object>();
				unwindData.put("followerId", followerId);
				// unwindData.put("leaderScreenName", leaderScreenName);
				unwindData.put("leaderTwitterId", followed.getTwitterId());
				unwindData.put("color", followed.getColor());
				unwindData.put("level", followed.getLevel() + 1);
				unwindList.add(unwindData);

				if (unwindList.size() > 999) {
					Map<String, Object> valueMap = new HashMap<String, Object>();
					valueMap.put("data", unwindList);

					peopleDao.saveFollowers(valueMap);

					log.info("Saving " + count + " followers of leader: " + followed);
					unwindList = new ArrayList<Map<String, Object>>();
				}
			}

			if (unwindList.size() > 0) {
				Map<String, Object> valueMap = new HashMap<String, Object>();
				valueMap.put("data", unwindList);

				peopleDao.saveFollowers(valueMap);

				log.info("Saving {} followers of leader: {}", count, followed);
			}

		} catch (Exception e) {
			log.error(e);
		}

		log.info("End saving follower of: {}. Page number: {}", followed, followed.getPageNum());

	}

	public void getInfoAndSaveUsers(List<Person> people) throws TwitterException {

		List<Long> ids = new ArrayList<Long>();

		for (int i = 0; i < people.size(); i++) {

			ids.add(people.get(i).getTwitterId());

			if (ids.size() == 100) {

				log.info("Retrieving users info");

				List<Person> filledPeople = twitterDao.getUsersDetail(ids);

				log.info("Users info retrieved from twitter");

				log.info("Saving users info to neo4j");

				peopleDao.saveUserDetail(filledPeople);

				log.info("Users info saved to neo4j");

				ids = new ArrayList<Long>();
			}

		}

		if (ids.size() > 0) {

			log.info("Retrieving users info");

			List<Person> filledPeople = twitterDao.getUsersDetail(ids);

			log.info("Users info retrieved from twitter");

			log.info("Saving users info to neo4j");

			peopleDao.saveUserDetail(filledPeople);

			log.info("Users info saved to neo4j");

		}
	}

	
    public void insertPerson(Map<String, String> data) {
    	
    	Person p = new Person();
    	
    	p.setLevel(0);
    	p.setTwitterId(Long.parseLong(data.get("twitterId")));
    	p.setColor(data.get("color"));
    	p.setType(data.get("type"));
    	p.setScreenName(data.get("screenName"));
    	
    	peopleDao.insertPerson(p);
    	
    }

}
