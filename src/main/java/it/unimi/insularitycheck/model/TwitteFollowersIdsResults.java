package it.unimi.insularitycheck.model;

public class TwitteFollowersIdsResults {

	private long nextCursor;
	private long[] ids;

	public long getNextCursor() {
		return nextCursor;
	}
	public void setNextCursor(long nextCursors) {
		this.nextCursor = nextCursors;
	}
	public long[] getIds() {
		return ids;
	}
	public void setIds(long[] ids) {
		this.ids = ids;
	}
	
	
}
