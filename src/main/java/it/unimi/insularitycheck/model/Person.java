package it.unimi.insularitycheck.model;

import java.util.Date;

public class Person {

	long twitterId;
	String screenName;
	Date lastFollowersRetrieving;
	int level;
	String type;
	String color;
	long nextCursor;
	int pageNum;
	int followersCount;
	int friendsCount;
	String location;
	String name;

	@Override
	public String toString() {
		return "twitterId: " + this.twitterId + ", screenName: " + this.screenName;
	}
	
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public long getTwitterId() {
		return twitterId;
	}
	public void setTwitterId(long twitterId) {
		this.twitterId = twitterId;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public Date getLastFollowersRetrieving() {
		return lastFollowersRetrieving;
	}
	public void setLastFollowersRetrieving(Date lastFollowersRetrieving) {
		this.lastFollowersRetrieving = lastFollowersRetrieving;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getNextCursor() {
		return nextCursor;
	}
	public void setNextCursor(long nextCursor) {
		this.nextCursor = nextCursor;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getFollowersCount() {
		return followersCount;
	}
	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}
	public int getFriendsCount() {
		return friendsCount;
	}
	public void setFriendsCount(int friendsCount) {
		this.friendsCount = friendsCount;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
