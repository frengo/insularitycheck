package it.unimi.insularitycheck.dal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterDriver {

	final Logger log = LogManager.getLogger();
	
	private static final String OAUTH_CONSUMER_KEY = "2rLvX4HddrtkepoRvGlwp2qmz";
	private static final String OAUTH_CONSUMER_SECRET = "yXSeo2d5jGReTVmFbeF5GxDbrmv0rSrzBRRZUpDDZ08e7z1dvN";
	private static final String OAUTH_ACCESS_TOKEN = "1001944954212667397-4FVMnX64aDotFWOP8rHFQYYKTdmjit";
	private static final String OAUTH_ACCESS_TOKEN_SECRET = "g1UPZbsNldzeGvgIVpxcvLSPYRlH2OW56tKGFx2mFatSo";

	
	private static TwitterDriver instance;
	private Twitter twitter;

	
	private TwitterDriver() {	
		
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setOAuthConsumerKey(OAUTH_CONSUMER_KEY)
          .setOAuthConsumerSecret(OAUTH_CONSUMER_SECRET)
          .setOAuthAccessToken(OAUTH_ACCESS_TOKEN)
          .setOAuthAccessTokenSecret(OAUTH_ACCESS_TOKEN_SECRET);
        TwitterFactory tf = new TwitterFactory(cb.build());
        this.twitter = tf.getInstance();    			

	}
	
	public static TwitterDriver getInstance() {
		if (instance == null) {
			instance = new TwitterDriver(); 
		}
		return instance;
	}
	
	public Twitter getTwitter() {
		return instance.twitter;
	}

}
