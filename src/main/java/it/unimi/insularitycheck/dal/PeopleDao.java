package it.unimi.insularitycheck.dal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Values;
import org.springframework.stereotype.Service;

import it.unimi.insularitycheck.model.Person;

@Service
public class PeopleDao {

	final Logger log = LogManager.getLogger();


	public List<Person> getPeopleWithSomeFollowers(int level, int followesThreashold) {

		Session s = Neo4jDriver.getInstance().getDriver().session(); 
    	StatementResult r = s.run("match (n:Person) where n.level = {level} and n.followersCount > {c} return n limit 500", Values.parameters("level", level, "c", followesThreashold));
    	
    	List<Person> p = fillPeople(r);    	
    	s.close();
    	
    	return p;
	}

	public List<Person> getPeopleNotLookedUpYet(int limit) {

    	Session s = Neo4jDriver.getInstance().getDriver().session(); 
    	StatementResult r = s.run("match (n:Person) where n.hasBeenLookedUp is null or n.hasBeenLookedUp = false return n limit "+ limit);
    	
    	List<Person> p = fillPeople(r);    	
    	s.close();
    	
    	return p;
	}
	
	
	public boolean savePerson(Person p) {

    	Session s = Neo4jDriver.getInstance().getDriver().session(); 
    	StatementResult r = s.run("match (n:Person) where n.twitterId = {id} set n.pageNum = {pn} ", Values.parameters("id", p.getTwitterId(), "pn", p.getPageNum()));
   
    	s.close();
    	
    	return true;
	}	
	
	
	public boolean insertPerson(Person p) {

    	Session s = Neo4jDriver.getInstance().getDriver().session(); 
    	StatementResult r = s.run(""
    			+ "create (n:Person{"
	    			+ "twitterId: {id}, "
	    			+ "screenName: {name}, "
	    			+ "level: {level}, "
	    			+ "color: {color}, "
	    			+ "type: {type} "
    			+ "}) ", Values.parameters(
    					"id", p.getTwitterId(), 
    					"name", p.getScreenName(),
    					"level", p.getLevel(),
    					"color", p.getColor(),
    					"type", p.getType()
    					));
   
    	s.close();
    	
    	return true;
	}

	public void saveFollowers(Map<String, Object> valueMap) {
		
    	Session s = Neo4jDriver.getInstance().getDriver().session();
    	
    	s.run("UNWIND {data} as row "
    			+ " MERGE (n:Person {twitterId: row.followerId} ) "
				+ " WITH n,row "
				+ " set n.color = coalesce(n.color,\"\") + \"|\" + row.color, "
				+ " n.level = case when n.level is not null then n.level else row.level end "
				+ " WITH row "
				+ " MATCH (f:Person),(l:Person) "
				+ " WHERE f.twitterId = row.followerId and l.twitterId = row.leaderTwitterId " 
				+ " MERGE (f)-[r:follows]->(l) ", valueMap);		
	
    	s.close();
	}

	public void saveUserDetail(List<Person> people) {
		
        List<Map<String, Object>> unwindList = new ArrayList<Map<String, Object>>();
        
    	for (Person p : people) {

    		Map<String, Object> unwindData = new HashMap<String, Object>();

    		unwindData.put("followerId", p.getTwitterId());
    		unwindData.put("friendsCount", p.getFriendsCount());
    		unwindData.put("followersCount", p.getFollowersCount());
    		unwindData.put("location", p.getLocation());
    		unwindData.put("screenName", p.getScreenName());
    		unwindData.put("name", p.getName());
    		unwindList.add(unwindData);

    	}		
		
		Map<String, Object> valueMap = new HashMap<String, Object>();
		valueMap.put("data", unwindList);
		
    	Session s = Neo4jDriver.getInstance().getDriver().session();
    	
    	s.run("UNWIND {data} as row "
    			+ " MATCH (n:Person {twitterId: row.followerId} ) "
				+ " SET "
					+ "	n.hasBeenLookedUp = true, "
					+ "	n.name = row.name, "
					+ " n.friendsCount = row.friendsCount, "
					+ " n.followersCount = row.followersCount, "
					+ " n.location = row.location, "
					+ " n.screenName = row.screenName ", valueMap);		
	
    	
    	s.close();
	}
	
	private List<Person> fillPeople(StatementResult r) {
		
		List<Person> people = new ArrayList<Person>();
		
    	while (r.hasNext()) {
        	Record record = r.next();
        	
        	Map<String, Object> map = record.get("n").asMap();
        	
        	Person p = new Person();
        	p.setTwitterId(Long.parseLong(map.get("twitterId").toString()));
        	
        	if (map.containsKey("nextCursor")) {
        		p.setNextCursor(Long.parseLong(map.get("nextCursor").toString()));
        	}
        	
        	if (map.containsKey("followersCount")) {
        		p.setFollowersCount(Integer.parseInt(map.get("followersCount").toString()));	
        	}
        	if (map.containsKey("level")) {
        		p.setLevel(Integer.parseInt(map.get("level").toString()));	
        	}
        	if (map.containsKey("pageNum")) {
        		p.setPageNum(Integer.parseInt(map.get("pageNum").toString()));	
        	} else {
        		p.setPageNum(1);
        	}

        	p.setType(String.valueOf(map.get("type")));
        	p.setScreenName(String.valueOf(map.get("screenName")));
        	
//        	Date lastFollowersRetrieving = (Date)record.get("n").get("lastFollowersRetrieving", new Date());
//        	p.setLastFollowersRetrieving(lastFollowersRetrieving);
        	
        	people.add(p);
    	}
    	
    	return people;
		
	}
	
}
