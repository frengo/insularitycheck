package it.unimi.insularitycheck.dal;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.unimi.insularitycheck.model.Person;
import it.unimi.insularitycheck.model.TwitteFollowersIdsResults;
import twitter4j.IDs;
import twitter4j.ResponseList;
import twitter4j.TwitterException;
import twitter4j.User;

@Service
public class TwitterDao {

	final Logger log = LogManager.getLogger();

	private static final String TWITTER_FILE_CACHE_FOLDER_PATH = "C:\\Users\\marco\\Documents\\U\\tesi\\followers";

	private LinkedList<Date> fWIndowExpireDates = new LinkedList<Date>();
	private LinkedList<Date> udWIndowExpireDates = new LinkedList<Date>();
	
	public synchronized TwitteFollowersIdsResults getNextFollowersIdsPageOf(long twitterId, long nextCursor, int pagNum)
			throws TwitterException {

		try {
			String cacheFileName = TWITTER_FILE_CACHE_FOLDER_PATH + "\\" + twitterId + "_" + pagNum;
			log.info("Search cache file = {}", cacheFileName);

			File cachefile = new File(cacheFileName);

			if (cachefile.exists()) {
				log.info("Esiste un file di cache");
				ObjectMapper mapper = new ObjectMapper();
				// Object to JSON in String
				TwitteFollowersIdsResults result = mapper.readValue(cachefile, TwitteFollowersIdsResults.class);
				return result;
			}
		} catch (Exception e) {
			log.error("Unable to read data from cache", e);
		}

		Date fifteenMinutesAgo = new Date(new Date().getTime() - (15 * 60 * 1000)); // 15 minutes ago..

		long apiCallsInTheLastFifteenMinutes = fWIndowExpireDates.stream()
				.filter(line -> fifteenMinutesAgo.before(line)).count();

		log.info("API calls in the last 15 minutes: {}", apiCallsInTheLastFifteenMinutes);

		if (apiCallsInTheLastFifteenMinutes > 14) {
			try {
				log.info("API call limit reached. Sleep a while..");
				Thread.sleep(30 * 1000); // 30 secs
			} catch (Exception e) {
			}

			// retry
			getNextFollowersIdsPageOf(twitterId, nextCursor, pagNum);
		}


		IDs followersIDs = TwitterDriver.getInstance().getTwitter().getFollowersIDs(twitterId, nextCursor);
		long[] ids = followersIDs.getIDs();
		nextCursor = followersIDs.getNextCursor();

		fWIndowExpireDates.add(new Date());
		
		TwitteFollowersIdsResults result = new TwitteFollowersIdsResults();
		result.setIds(ids);
		result.setNextCursor(nextCursor);

		try {
			File cacheFile = new File(TWITTER_FILE_CACHE_FOLDER_PATH + "\\" + twitterId + "_" + pagNum);
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(cacheFile, result);
		} catch (Exception e) {
			log.error("Unable to save result to file for caching purpose", e);
		}

		return result;
	}
	

	public synchronized List<Person> getUsersDetail(List<Long> twitterIds) throws TwitterException {

		Date fifteenMinutesAgo = new Date(new Date().getTime() - (15*60*1000)); // 15 minutes ago..
		
		long apiCallsInTheLastFifteenMinutes = udWIndowExpireDates.stream()
	                .filter(line -> fifteenMinutesAgo.before(line))
	                .count();
		
		log.info("API calls in the last 15 minutes: {}", apiCallsInTheLastFifteenMinutes);
		
		if (apiCallsInTheLastFifteenMinutes > 899) {
			try {
				log.info("API call limit reached. Sleep a while..");
				Thread.sleep(30*1000); // 30 secs
			} catch(Exception e) {}
			
			// retry
			getUsersDetail(twitterIds);
		}
		

		
	    long[] ret = new long[twitterIds.size()];
	    Iterator<Long> iterator = twitterIds.iterator();
	    for (int i = 0; i < ret.length; i++)
	    {
	        ret[i] = iterator.next().intValue();
	    }

	    udWIndowExpireDates.add(new Date());
	    
		List<Person> people = new ArrayList<Person>();
    	ResponseList<User> users = TwitterDriver.getInstance().getTwitter().lookupUsers(ret);
    	for (User s : users) {
    		Person p = new Person();
    		p.setFollowersCount(s.getFollowersCount());
    		p.setFriendsCount(s.getFriendsCount());
    		p.setScreenName(s.getScreenName());
    		p.setName(s.getName());
    		p.setLocation(s.getLocation());
    		p.setTwitterId(s.getId());
    		people.add(p);
    	}
    	
    	return people;
	}
		

}
