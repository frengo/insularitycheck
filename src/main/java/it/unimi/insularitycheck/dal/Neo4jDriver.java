package it.unimi.insularitycheck.dal;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;

public class Neo4jDriver {

	final Logger log = LogManager.getLogger();

	private static final String URI = "bolt://localhost:7687";
	private static final String USER = "neo4j";
	private static final String PWD = "insularity";
	
	private static Neo4jDriver instance;
	private Driver driver;
	
	private Neo4jDriver() {	
		this.driver = GraphDatabase.driver( URI, AuthTokens.basic( USER, PWD), 
				Config.build().withMaxTransactionRetryTime(15, TimeUnit.SECONDS).toConfig());
	}
	
	public static Neo4jDriver getInstance() {
		if (instance == null) {
			instance = new Neo4jDriver(); 
		}
		return instance;
	}
	
	public Driver getDriver() {
		return instance.driver;
	}
	
	public void close() {
		log.info("Closing neo4j session");
		instance.driver.close();
		log.info("Neo4j session closed");
	}
	
}
