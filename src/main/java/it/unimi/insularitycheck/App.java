package it.unimi.insularitycheck;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.swing.plaf.synth.SynthSplitPaneUI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.springframework.boot.Banner.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import it.unimi.insularitycheck.controller.PersonController;
import it.unimi.insularitycheck.dal.TwitterDriver;
import it.unimi.insularitycheck.services.PersonService;
import twitter4j.TwitterException;


@SpringBootApplication
@EnableCaching
public class App implements CommandLineRunner
{

	final Logger log = LogManager.getLogger();
	

	private TwitterDriver twitter;

	
//	private static ExecutorService outerExecutor = Executors.newFixedThreadPool(3);
	private ExecutorService innerExecutor;
	
	@Autowired
	private PersonService appService;

	
    public static void main( String[] args )
    {

        SpringApplication app = new SpringApplication(App.class);
        app.setBannerMode(Mode.OFF);
        app.run(args);
    }
    
    /**
     * 
     * @param args: 
     * 1* leader screen name
     * 2* twitter id
     * 3* leader color
     * 4* number of thread that simultaneously retrieve followers from twitter and save them to DB
     * 5° resuming?
     */
    public void run(String... args) {

    	// TODO gestire il level
    	
        // create a scanner so we can read the command-line input
        Scanner scanner = new Scanner(System.in);
        
        String command = null;

        do {
	        printCommand();
	
	        // get their input as a String
	        command = scanner.nextLine();

        } while (command == null || command.isEmpty() || !(command.startsWith("import") || command.startsWith("lookup") || command.startsWith("insert")));
        
        args = command.split(" ");    	
    	
    	if (args.length < 1) {
    		throw new IllegalArgumentException("At least one parameter must be provided");
    	}
    	
    	this.twitter = TwitterDriver.getInstance();

    	try {
    		
	    	String choice = args[0];

	    	switch (choice) {
			case "import":
		    	if (args.length < 3) {
		    		throw new IllegalArgumentException("Level and followers count threashold are mandatory");
		    	}
		    	int level = Integer.parseInt(args[1]);
		    	int followersCountThreashold = Integer.parseInt(args[2]);
				
				// importa i followers delle persone di primo livello (LEVEL=0)
		    	appService.importFollowers(level, followersCountThreashold);			
				break;
			case "lookup":
				log.info(args.length);
		    	if (args.length < 2) {
		    		throw new IllegalArgumentException("Limit is mandatory");
		    	}
		    	int limit = Integer.parseInt(args[1]);
				// recupera il dettaglio degli utenti acquisiti
		    	appService.lookUpPeople(limit);
				break;
			
			case "insert":
				log.info(args.length);
				Map<String, String> data = new HashMap<String, String>();
		    	for (int i=1;i<args.length;i++) {
		    		String[] attr = args[i].split("=");
		    		data.put(attr[0], attr[1]);
		    	}
		    	
				// recupera il dettaglio degli utenti acquisiti
		    	appService.insertPerson(data);
				break;

			} 
	    	
		} catch (TwitterException e) {
			log.error(e);
		}

    	
    }
    
    private void printCommand() {
        //  prompt for the user's name
        System.out.print("commands: \n"
        		+ "- import [level] [followers-count-threashold] \n"
        		+ "- lookup \n"
        		+ ": ");
    }
    

    
    
    /** ----------------------------------------------------------------------------------- **/

}
